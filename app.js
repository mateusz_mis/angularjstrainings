var app = angular.module('myApp', []);

app.controller('DemoCtrl', function() {

  this.compareJson = () => {
    this.prepareData();

    if (!!!this.jsonOneParsed  || !!!this.jsonTwoParsed) {
        this.errorOccured = 'Empty JSON';
    } else {
        this.compareKeys(this.jsonOneParsed, this.jsonTwoParsed);

        if(!this.errorOccured) {
            this.compareSuccess = true;
            this.verifyChild();
        }
    }

  }
// const name = name || null; 


  this.compareKeys = (obj1, obj2) => {
    this.keysOne = this.getJsonKeys(obj1);
    this.keysTwo = this.getJsonKeys(obj2);
    console.log('keysOne', this.keysOne, 'keysTwo', this.keysTwo);

    for (let i = 0 ; i < this.keysOne.length; i++ ) {
        if (this.keysTwo.indexOf(this.keysOne[i]) === -1) {
            this.errorOccured = this.keysOne[i];
        }
    }
  }


  this.getKeysWithObject = (name = null) => {
      if (!name) {
          name = this.jsonOneParsed;
      }

    let keysList = [];
    let keys = this.getJsonKeys(name);

    for (let i = 0 ; i < keys.length; i++ ) {
        if (typeof name[keys[i]] === 'object') {
            keysList.push(this.keysOne[i]);
        }
    }

    return keysList;
  }



  this.verifyChild = (name = null) => {
      console.log("name in : ", name);
      if (!name) {
        let keysList = this.getKeysWithObject();
        let keysChildList = [];

        if(keysList.length > 0 ) {
            for (let j = 0 ; j < keysList.length; j++ ) {
                this.compareKeys(this.jsonOneParsed[keysList[j]], this.jsonTwoParsed[keysList[j]]);
                
                keysChildList = this.getKeysWithObject(this.jsonOneParsed[keysList[j]]);
                if (keysChildList.length > 0) {
                    console.log("keysChildList: ",keysList)
                    this.verifyChild([keysList[j], keysChildList])
                }
            }
        }
    } else {
        if(name[0] && name[1]) {
            console.log('parent: ', name[0], 'childList:', name[1]);
            let temp1 = this.jsonOneParsed;
            let temp2 = this.jsonTwoParsed;
            for (let i = 0 ; i < name[1].length ; i++) {
                temp1 = temp1[name[0]][name[1][i]];
                temp2 = temp2[name[0]][name[1][i]];
            }
            console.log("temp1", temp1, 'temp2', temp2);
            this.compareKeys(temp1, temp2);
            keysChildList = this.getKeysWithObject(temp1)
            console.log('keysChildList x ', keysChildList);

            if (keysChildList.length > 0) {
                console.log("keysChildList: ", name[0])

                //this.verifyChild([name[0], keysChildList])
            }
            
        } else {
            let childObjectPath1 = this.jsonOneParsed;
            let childObjectPath2 = this.jsonTwoParsed;
            for(let i = 0; i< name.length; i++) {
                childObjectPath1 = childObjectPath1[name[i]];
                childObjectPath2 = childObjectPath2[name[i]];
    
            }
            this.compareKeys(childObjectPath1, childObjectPath2);
        }

    }
  }



    this.compareChildKeys = (childObject, childKeys) => {
        childKeys.forEach((key) => {
            if (!this.jsonTwoParsed[childObject][key]){
                this.errorOccured = key;
            }
        })
    }

this.getJsonKeys = (child) => {
    let tempKeys = [];
    for (let key in child) {
        tempKeys.push(key)
    }
    return tempKeys;
  }


  this.parseToJson = () => {
    if (this.jsonOne) {
        this.jsonOne = this.jsonOne.replace(/'/g, "\"");
        this.jsonOneParsed = angular.fromJson(this.jsonOne);
        this.jsonOne = JSON.stringify(this.jsonOneParsed, null, "\t");
    }
    if (this.jsonTwo) {
        this.jsonTwo = this.jsonTwo.replace(/'/g, "\"");
        this.jsonTwoParsed = angular.fromJson(this.jsonTwo);
        this.jsonTwo = JSON.stringify(this.jsonTwoParsed, null, "\t");
    }
  }

  this.prepareData = () => {
    this.errorOccured = '';
    this.compareSuccess = false;
    this.keysOne = [];
    this.keysTwo = [];
    this.jsonOneParsed = '';
    this.jsonTwoParsed = '';

    this.parseToJson();
  }

});
